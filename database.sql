CREATE DATABASE IF NOT EXISTS addison;
 
USE addison;
 
DROP TABLE IF EXISTS user;
 
CREATE TABLE user (
	user_id  varchar(255) NOT NULL,
	password VARCHAR (255) NOT NULL,
  	PRIMARY KEY (user_id)
);
