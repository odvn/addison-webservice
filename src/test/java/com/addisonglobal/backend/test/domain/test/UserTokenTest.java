/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addisonglobal.backend.test.domain.test;

import com.addisonglobal.backend.test.domain.UserToken;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * UserTokenTest class. Class to test the user token.
 * @author ulabovn
 */
public class UserTokenTest {

    /**
     * User object to be tested.
     */
    private UserToken user;

    public UserTokenTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * The user is initialized in every test.
     */
    @Before
    public void setUp() {
        user = new UserToken("ulabovn");
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test the set/get of the user id.
     */
    @Test
    public void assertEqualsUserIdTest() {
        user.setUserId("ulabtest");
        Assert.assertEquals("ulabtest", user.getUserId());
    }

    /**
     * Test the set/get of the user token.
     */
    @Test
    public void assertEqualsTokenTest() {
        user = new UserToken("ulabtest");
        String token = user.getToken();
        String userId = token.substring(0, token.indexOf('_'));
        String currentDate = token.substring(token.indexOf('_') + 1);
        Assert.assertEquals("ulabtest", userId);
        DateFormat format = new SimpleDateFormat("yyyy-MM-ddhh:mm:ss.SSS");
        format.setLenient(false);
        try {
            format.parse(currentDate);
            Assert.assertTrue(true);
        } catch (ParseException e) {
            fail("Invalid token");
        }
    }
}
