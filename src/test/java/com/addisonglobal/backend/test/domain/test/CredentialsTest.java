/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addisonglobal.backend.test.domain.test;

import com.addisonglobal.backend.test.domain.Credentials;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author ulabovn
 */
public class CredentialsTest {

    Credentials credentials;

    public CredentialsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        credentials = new Credentials();
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
     @Test
     public void assertCredentialsUserNameTest() {
         credentials.setUserName("ulabovn");
         Assert.assertEquals("ulabovn", credentials.getUsername());
     }
     
     @Test
     public void assertCredentialsPasswordTest() {
        credentials.setPassword("onetwo12");
        Assert.assertEquals("onetwo12", credentials.getPassword());
     }
}
