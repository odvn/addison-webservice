/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addisonglobal.backend.test.domain.test;

import com.addisonglobal.backend.test.domain.User;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author ulabovn
 */
public class UserTest {

    /**
     * User object to be tested.
     */
    User user;

    public UserTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * The user is set in all tests.
     */
    @Before
    public void setUp() {
        user = new User("ulabovn");
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void assertUserIdTest() {
        user.setUserId("ulabtest");
        Assert.assertEquals("ulabtest", user.getUserId());
    }
}
