/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addisonglobal.backend.test.servlets;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * ContextListener class.
 * @author Oscar del Valle Narv�ez
 */
public class ContextListener implements ServletContextListener {
    
    /**
     * ContextListener constructor.
     */
    public ContextListener() {
        // Default empty constructor
    }

    /**
     * Initialize the service.
     * @param sce Server context.
     */
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        
    }

    /**
     * Destroy the service.
     * @param sce Server context.
     */
    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }
}

