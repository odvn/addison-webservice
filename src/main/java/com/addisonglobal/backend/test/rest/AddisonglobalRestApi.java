/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addisonglobal.backend.test.rest;

import com.addison.persistence.dao.UserSystemDAO;
import com.addison.persistence.dao.UserSystemDAOImpl;
import com.addison.persistence.entity.UserSystemEntity;
import com.addisonglobal.backend.test.domain.Credentials;
import com.addisonglobal.backend.test.domain.UserToken;
import com.addisonglobal.backend.test.domain.log.LogMessages;
import com.addisonglobal.backend.test.service.Service;
import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import org.hibernate.internal.util.config.ConfigurationException;

/**
 * Addison Global Test Rest API.
 * @author Oscar del Valle Narvaez
 */
@Path("/")
public class AddisonglobalRestApi {
   
    @PUT
    @Path("adduser")
    @Consumes("application/json")
    @Produces("application/json")
    public Response addUser(Credentials credentials) {
        Response response;
        ResponseBuilder responseBuilder;
        try {
            UserSystemEntity userObj = new UserSystemEntity();
            userObj.setUserName(credentials.getUsername());
            userObj.setPassword(credentials.getPassword());
            UserSystemDAO dao = new UserSystemDAOImpl();
            dao.setSessionFactory(Service.sessionFactory());
            if (dao.addUser(userObj)) {
                responseBuilder = Response.status(Response.Status.OK);
                responseBuilder = responseBuilder
                        .entity("User added to the system");
                response = responseBuilder.build();
            } else {
                responseBuilder = Response.status(Response.Status
                        .INTERNAL_SERVER_ERROR);
                responseBuilder = responseBuilder
                        .entity("Cannot add the user in the system");
                response = responseBuilder.build();
            }
        } catch (ConfigurationException ex) {
            responseBuilder = Response.status(Response.Status
                    .INTERNAL_SERVER_ERROR);
            responseBuilder = responseBuilder
                    .entity("Internal database error" + ex.getMessage());
            response = responseBuilder.build();
        }
        return response;
    }

    @POST
    @Path("delete")
    @Consumes("application/json")
    @Produces("application/json")
    public Response deleteUser(Credentials credentials) {
        Response response;
        ResponseBuilder responseBuilder;
        UserSystemEntity userObj = new UserSystemEntity();
        userObj.setUserName(credentials.getUsername());
        userObj.setPassword(credentials.getPassword());
        UserSystemDAO dao = new UserSystemDAOImpl();
        dao.setSessionFactory(Service.sessionFactory());
        if (dao.removeUser(userObj)) {
            responseBuilder = Response.status(Response.Status.OK);
            responseBuilder = responseBuilder
                    .entity("User added to the system");
            response = responseBuilder.build();
        } else {
            responseBuilder = Response.status(Response.Status
                    .INTERNAL_SERVER_ERROR);
            responseBuilder = responseBuilder
                    .entity("Cannot add the user in the system");
            response = responseBuilder.build();
        }
        return response;
    }

    @POST
    @Path("/issueToken")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response issueToken(@Context HttpServletRequest req,
            Credentials credentials) {
        Response response;
        ResponseBuilder responseBuilder;
        try {
            Service service = new Service();
            CompletableFuture<UserToken> userToken = service.issueToken(credentials);
            while (!userToken.isDone()) {
                // Waits a random delay between 1 and 5000 milliseconds.
                Thread.sleep(new Random().nextInt(5000-1) + 1);
            }
            if (userToken.get() != null) {
                UserToken token = userToken.get();
                responseBuilder = Response.status(Response.Status.OK);
                responseBuilder.entity("Permission granted. User token: "
                        + token.getToken());
                response = responseBuilder.build();
            } else {
                responseBuilder = Response.status(Response.Status.FORBIDDEN);
                responseBuilder = responseBuilder
                        .entity("The user is not allow to use the system");
                response = responseBuilder.build();
            }
        } catch (InterruptedException ex) {
            responseBuilder = Response.status(Response.Status
                    .INTERNAL_SERVER_ERROR);
            responseBuilder = responseBuilder
                    .entity(LogMessages.INTERRUPTED_EXCEPTION);
            response = responseBuilder.build();
        } catch (ExecutionException | UnsupportedOperationException ex) {
            responseBuilder = Response.status(Response.Status
                    .INTERNAL_SERVER_ERROR);
            responseBuilder = responseBuilder
                    .entity(LogMessages.EXECUTE_EXCEPTION);
            response = responseBuilder.build();
        }
        return response;
    }
}
