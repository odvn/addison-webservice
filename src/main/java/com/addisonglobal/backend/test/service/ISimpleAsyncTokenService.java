/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addisonglobal.backend.test.service;

import com.addisonglobal.backend.test.domain.Credentials;
import com.addisonglobal.backend.test.domain.UserToken;
import java.util.concurrent.CompletableFuture;

/**
 * ISimpleAsyncTokenService class.
 * @author Oscar del Valle Narvaez
 */
public interface ISimpleAsyncTokenService {

    /**
     * Method that get asynchronously a user token.
     * @param credentials The user credentials.
     * @return A future action that get the user token.
     */
    default CompletableFuture<UserToken> issueToken(Credentials credentials) {
        CompletableFuture<UserToken> future = CompletableFuture.supplyAsync(() -> {
            UserToken userToken;
            if (credentials.getUsername().equals(credentials.getPassword())) {
                Service service = new Service();
                userToken = service.getUserToken(credentials);
            } else {
                userToken = null;
            }
            return userToken;
        });
        return future;
    }
    
}
