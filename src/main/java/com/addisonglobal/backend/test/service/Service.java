/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addisonglobal.backend.test.service;

import com.addison.persistence.dao.UserSystemDAO;
import com.addison.persistence.dao.UserSystemDAOImpl;
import com.addison.persistence.entity.UserSystemEntity;
import com.addisonglobal.backend.test.domain.Credentials;
import com.addisonglobal.backend.test.domain.User;
import com.addisonglobal.backend.test.domain.UserToken;
import org.hibernate.SessionFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.orm.hibernate3.AbstractSessionFactoryBean;
import org.springframework.orm.hibernate3.annotation.AnnotationSessionFactoryBean;

/**
 * Service class.
 * @author ulabovn
 */
public class Service implements ISimpleAsyncTokenService {

    private static SessionFactory sessionFactoryObj;

    /**
     * Service constructor.
     */
    public Service() {
        // Default service constructor.
    }

    @Bean
    public static AbstractSessionFactoryBean sessionFactoryBean(){
        AnnotationSessionFactoryBean sessionFactoryBean = new AnnotationSessionFactoryBean();
        sessionFactoryBean.setConfigLocation(new ClassPathResource("META-INF/application-context.xml"));
        System.out.println(sessionFactoryBean.toString());
        return sessionFactoryBean;
    }
    
    public UserToken getUserToken(Credentials credentials) {
        UserSystemEntity userObj = new UserSystemEntity();
        userObj.setUserName(credentials.getUsername());
        userObj.setPassword(credentials.getPassword());
        UserSystemDAO dao = new UserSystemDAOImpl();
        dao.setSessionFactory(Service.sessionFactory());
        UserSystemEntity userDB = dao.getUserByUserName(credentials.getUsername());
        User user = new User(userDB.getUserName());
        UserToken userToken = new UserToken(user.getUserId());
        return userToken;
    }
    
    public static SessionFactory sessionFactory() {
        ApplicationContext context = new ClassPathXmlApplicationContext("META-INF/application-context.xml");
        return (SessionFactory) context.getBean("sessionFactory");
    }
}
