/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addisonglobal.backend.test.service;

import com.addisonglobal.backend.test.domain.Credentials;
import com.addisonglobal.backend.test.domain.User;
import com.addisonglobal.backend.test.domain.UserToken;
import com.addisonglobal.backend.test.domain.log.LogMessages;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * IAsyncTokenService class
 * @author Oscar del Valle Narvaez
 */
public interface IAsyncTokenService {

    CompletableFuture<User> authenticate(Credentials credentials);

    CompletableFuture<UserToken> requestToken(User user);

    default Future<UserToken> issueToken(Credentials credentials) {
        final CompletableFuture<UserToken> completableFuture
                = new CompletableFuture<>();
        Executors.newCachedThreadPool().submit(() -> {
            try {
                CompletableFuture<User> user = authenticate(credentials);
                if (user.get().isValidUser()) {
                    completableFuture.complete(requestToken(user.get()).get());
                    final UserToken token = completableFuture.get();
                    // Small delay to be set between user request and its result.
                    Thread.sleep(50);
                    return completableFuture.toCompletableFuture();
                } else {
                    return completableFuture;
                }
            } catch (InterruptedException ex) {
                // If the current Thread has been interrupted.
                // Should never enter here.
                Logger.getLogger(IAsyncTokenService.class.getName()).log(Level
                .SEVERE, LogMessages.INTERRUPTED_EXCEPTION, ex);
                completableFuture.completeExceptionally(ex);
            } catch (ExecutionException | UnsupportedOperationException ex) {
                // If there is an error executing the request.
                Logger.getLogger(IAsyncTokenService.class.getName()).log(Level
                .SEVERE, LogMessages.EXECUTE_EXCEPTION, ex);
                completableFuture.completeExceptionally(ex);
            }
            return completableFuture;
        });
        return completableFuture;
    }
}