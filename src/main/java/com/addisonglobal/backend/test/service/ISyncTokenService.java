/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addisonglobal.backend.test.service;

import com.addisonglobal.backend.test.domain.Credentials;
import com.addisonglobal.backend.test.domain.User;
import com.addisonglobal.backend.test.domain.UserToken;
import com.addisonglobal.backend.test.domain.exceptions.InvalidUserException;
import com.addisonglobal.backend.test.domain.log.LogMessages;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * ISyncTokenService class.
 * @author Oscar del Valle Narvaez
 */
public interface ISyncTokenService {

    /**
     * Authenticate method.
     * @param credentials User credentials.
     * @return The user authenticated.
     */
    User authenticate(Credentials credentials);

    /**
     * Method that request the user token.
     * @param user The user to get the token.
     * @return The user token.
     */
    UserToken requestToken(User user);

    /**
     * Get the user token from the credentials of a user.
     * @param credentials User credentials
     * @return THe user token
     * @throws InvalidUserException If the user is invalid.
     */
    default UserToken issueToken(Credentials credentials)
            throws InvalidUserException {
        UserToken token;
        try {
            User user = authenticate(credentials);
            if (user.isValidUser()) {
                token = requestToken(user);
                return token;
            } else {
                throw new InvalidUserException("Invalid user");
            }
        } catch (UnsupportedOperationException ex) {
            // If there is an error executing the request.
            Logger.getLogger(IAsyncTokenService.class.getName()).log(Level
            .SEVERE, LogMessages.EXECUTE_EXCEPTION, ex);
            throw new InvalidUserException(LogMessages.INTERRUPTED_EXCEPTION);
        }
    }

}