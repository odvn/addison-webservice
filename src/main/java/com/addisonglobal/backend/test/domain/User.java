/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addisonglobal.backend.test.domain;

/**
 * User class. This class represent a user in the system.
 * @author Oscar del Valle Narvaez
 */
public class User {

    /**
     * User id.
     */
    private String userId;

    /**
     * Valid user.
     */
    private boolean validUser;
    /**
     * User constructor.
     * @param id User id.
     */
    public User(String id) {
        userId = id;
    }

    /**
     * Get the user id.
     * @return The user id.
     */
    public String getUserId(){
        return userId;
    }

    /**
     * Set the user id.
     * @param id User id.
     */
    public void setUserId(String id){
        userId = id;
    }

    /**
     * Set if the user is valid or not.
     * @param valid True if the user is valid, false otherwise.
     */
    public void setValidUser(boolean valid){
        validUser = valid;
    }

    /**
     * Check if the user is valid or not.
     * @return True if the user is valid, false otherwise.
     */
    public boolean isValidUser(){
        return validUser;
    }

    /**
     * ToString overridden method. This method represents the string value of
     * the class.
     * @return The string representation of the object
     */
    @Override
    public String toString() {
        return String.valueOf(userId);
    }
}
