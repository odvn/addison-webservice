/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addisonglobal.backend.test.domain.exceptions;

/**
 * InvalidUserException class.
 * @author Oscar del Valle Narvaez
 */
public class InvalidUserException extends Exception {

    /**
     * Serial version UID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * InvalidUserException constructor.
     * @param message The message of the exception
     */
    public InvalidUserException(String message) {
        super(message);
    }
}

