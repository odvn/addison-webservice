/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addisonglobal.backend.test.domain;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * UserToken class. This class gives the token of a user
 * @author Oscar del Valle Narvaez
 */
public class UserToken {

    /**
     * User related to the token.
     */
    private String userId;

    /**
     * The user token.
     */
    private String token;

    /**
     * UserToken constructor.
     * @param id User id. 
     */
    public UserToken(String id) {
        userId = id;
    }

    /**
     * Set user id related to the token.
     * @param id The user id related to the token.
     */
    public void setUserId(String id) {
        userId = id;
    }

    /**
     * Get the user id related to the token.
     * @return The user id related to the token.
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Get the user token.
     * @return The user token. The token is build by the user id and the current
     *         date in a certain format.
     */
    public String getToken() {
        DateTimeFormatter formatter = DateTimeFormatter
                .ofPattern("yyyy-MM-ddhh:mm:ss.SSS");
        LocalDateTime now = LocalDateTime.now();
        // Concats the current date
        token = userId.concat("_").concat(now.format(formatter));
        return token;
    }
}