/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addisonglobal.backend.test.domain;

/**
 * Credential class. This class is used to identify the user in order to
 * authenticate on the system.
 * @author Oscar del Valle Narvaez
 */
public class Credentials {

    /**
     * User name.
     */
    private String userName;

    /**
     * User password.
     */
    private String password;

    /**
     * Credentials constructor.
     */
    public Credentials() {
        // Default empty public constructor.
    }

    /**
     * Get the user name.
     * @return The user name.
     */
    public String getUsername(){
      return userName;
    }

    /**
     * Set the user name.
     * @param name The user name.
     */
    public void setUserName(String name) {
        userName = name;
    }

    /**
     * Get the user password.
     * @return The user password.
     */
    public String getPassword(){
      return password;
    }

    /**
     * Set the user password.
     * @param pass The user password.
     */
    public void setPassword(String pass) {
        password = pass;
    }
}
