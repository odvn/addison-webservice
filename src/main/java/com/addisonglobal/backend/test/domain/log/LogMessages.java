/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addisonglobal.backend.test.domain.log;

/**
 * LogMessages class.
 * @author Oscar del Valle Narvaez
 */
public class LogMessages {

    /**
     * Interrupted exception fail message
     */
    public static final String INTERRUPTED_EXCEPTION
            = "There was a concurrency error getting the user";

    /**
     * Execute exception fail message.
     */
    public static final String EXECUTE_EXCEPTION
            = "There was an internal error getting the user";

    /**
     * Default private empty constructor.
     */
    private LogMessages() {
        // Default empty constructor.
    }
}
